package net.thebugmc.parser.util;

import net.thebugmc.parser.util.FilePointer;

import java.util.function.Supplier;

/**
 * Whenever any issue in Parser occurs. Usually thrown using {@link #ASSERT(boolean, String)} and @{@link #ASSERT(boolean, Supplier, String)}.
 */
public class ParserException extends IllegalArgumentException {
    /**
     * Construct a Parser Exception without a file pointer.
     *
     * @param message Error message.
     */
    public ParserException(String message) {
        super(message);
    }

    /**
     * Make sure a condition is met, else throw an exception with a given message.
     *
     * @param check       Condition to ensure.
     * @param elseMessage Error message.
     * @throws ParserException When condition is not met.
     */
    public static void ASSERT(boolean check, String elseMessage) throws ParserException {
        if (!check) throw new ParserException(elseMessage);
    }

    /**
     * Construct a Parser Exception with a file pointer.
     *
     * @param message Error message.
     * @param ptr     Position in the file where Parser could not proceed.
     */
    public ParserException(String message, net.thebugmc.parser.util.FilePointer ptr) {
        super(message + " [" + ptr + "]");
    }

    /**
     * Make sure a condition is met, else throw an exception with a given message.
     *
     * @param check       Condition to ensure.
     * @param ptr         Position in the file where Parser could not proceed.
     * @param elseMessage Error message.
     * @throws ParserException When condition is not met.
     */
    public static void ASSERT(boolean check, net.thebugmc.parser.util.FilePointer ptr, String elseMessage) throws ParserException {
        if (!check) throw new ParserException(elseMessage, ptr);
    }

    /**
     * Make sure a condition is met, else throw an exception with a given message.
     *
     * @param check       Condition to ensure.
     * @param ptr         Position in the file where Parser could not proceed.
     * @param elseMessage Error message.
     * @throws ParserException When condition is not met.
     */
    public static void ASSERT(boolean check, Supplier<FilePointer> ptr, String elseMessage) throws ParserException {
        if (!check) throw new ParserException(elseMessage, ptr.get());
    }
}
