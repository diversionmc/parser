package net.thebugmc.parser.group;

import net.thebugmc.parser.group.GroupSupplier;
import net.thebugmc.parser.util.ParserException;
import net.thebugmc.parser.Parser;
import net.thebugmc.parser.expression.ExpressionPiece;

import java.util.List;
import java.util.function.Predicate;

import static net.thebugmc.parser.util.ParserException.ASSERT;

/**
 * <p>
 * When all characters are converted successfully into Expression Pieces,
 * some of the pieces are allowed to be converted again using a second pass.
 * This time sequences of pieces (Content) surrounded by
 * some specific pieces (Group Boundaries)
 * can be grouped up into lists (into Group Pieces).
 * </p>
 *
 * <p>
 * The mechanism that performs such an operation is called a Grouper.
 * It consists of two predicates (Start and End) which define boundaries for a group
 * and convert the content into the group pieces using a function called Group Supplier.
 * </p>
 *
 * @param <L> Start boundary.
 * @param <R> End boundary.
 */
@SuppressWarnings("unchecked")
public record Grouper<L extends net.thebugmc.parser.expression.ExpressionPiece, R extends net.thebugmc.parser.expression.ExpressionPiece>(Predicate<net.thebugmc.parser.expression.ExpressionPiece> start,
                                                                                                                                          Predicate<net.thebugmc.parser.expression.ExpressionPiece> end,
                                                                                                                                          net.thebugmc.parser.group.GroupSupplier<L, R> supplier) {
    /**
     * Construct a grouper for specific pieces.
     *
     * @param start    Start boundary check.
     * @param end      End bounary check.
     * @param supplier Group supplier.
     * @throws net.thebugmc.parser.util.ParserException If any of the parameters are null.
     * @see Parser#group(Predicate, Predicate, GroupSupplier)
     */
    public Grouper {
        net.thebugmc.parser.util.ParserException.ASSERT(start != null, "Start group piece predicate is null");
        net.thebugmc.parser.util.ParserException.ASSERT(end != null, "End group piece predicate is null");
        net.thebugmc.parser.util.ParserException.ASSERT(supplier != null, "Group supplier is null");
    }

    /**
     * Check if a piece matches the starting boundary of the group.
     *
     * @param toTest Piece to check.
     * @return True if a piece is a start boundary.
     */
    public boolean start(net.thebugmc.parser.expression.ExpressionPiece toTest) {
        return start.test(toTest);
    }

    /**
     * Check if a piece matches the ending boundary of the group.
     *
     * @param toTest Piece to check.
     * @return True if a piece is an end boundary.
     */
    public boolean end(net.thebugmc.parser.expression.ExpressionPiece toTest) {
        return end.test(toTest);
    }

    /**
     * Create a group from the given pieces.
     *
     * @param left    Left boundary. Make sure it matches {@link #start(net.thebugmc.parser.expression.ExpressionPiece)}
     * @param right   Right boundary. Make sure it matches {@link #end(net.thebugmc.parser.expression.ExpressionPiece)}
     * @param content Pieces inside the group.
     * @return Group made from the pieces.
     */
    public net.thebugmc.parser.expression.ExpressionPiece group(
        net.thebugmc.parser.expression.ExpressionPiece left, net.thebugmc.parser.expression.ExpressionPiece right,
        List<ExpressionPiece> content) {
        return supplier.apply((L) left, (R) right, content);
    }
}
