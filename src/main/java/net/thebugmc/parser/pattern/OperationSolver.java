package net.thebugmc.parser.pattern;

import net.thebugmc.parser.util.Pointable;
import net.thebugmc.parser.util.ParserException;

import java.util.List;
import java.util.function.Predicate;

import static net.thebugmc.parser.util.ParserException.ASSERT;


/**
 * <p>
 * When confronted with a pattern of kind &lt;value&gt;[&lt;operator&gt;&lt;value&gt;]...,
 * you do not need to completely rewrite a custom pattern,
 * as Parser comes with a tool specifically made for that.
 * </p>
 *
 * <p>
 * It reads the list (that contains both already parsed sentences for values and pieces for operators)
 * either right to left or left to right (some operators require that, like power “^”),
 * and reduces matching triplets of &lt;value&gt;&lt;operator&gt;&lt;value&gt; to a single &lt;value&gt; sentence.
 * </p>
 *
 * <p>
 * When creating a solver, you supply a predicate that defines an operator
 * (to check the type of the operator for the order of operations)
 * and a function that converts the triplet to a sentence.
 * </p>
 *
 * @param rightToLeft Whether the solver should run right to left (e.g. power, assign, etc) or left to right (e.g. sum, product, etc).
 * @param opCheck Check whether a given piece is an operator. Only even pieces in a file are checked.
 * @param operator Action that combines two values and an operator into a new resulting value.
 * @param <V> Input Value / Result type.
 * @param <O> Operator type.
 */
public record OperationSolver<V extends Pointable, O extends Pointable>(boolean rightToLeft,
                                                                        Predicate<O> opCheck,
                                                                        TriOperator<V, O> operator) {
    @SuppressWarnings("unchecked")
    public void solve(List<Pointable> list) {
        for (int i = rightToLeft ? list.size() - 2 : 1;
             rightToLeft == (i >= (rightToLeft ? 0 : list.size()));
             i += rightToLeft ? -2 : 2) {
            var op = (O) list.get(i);
            if (opCheck.test(op)) {
                V left = (V) list.remove(i - 1);
                list.remove(i - 1); // operator
                V right = (V) list.remove(i - 1);

                var res = operator.apply(left, right, op);
                ParserException.ASSERT(res != null, op.pointer(), "operation failed");
                list.add(i - 1, res);
                if (!rightToLeft) i -= 2;
            }
        }
    }

    /**
     * Operation between two values and an operator.
     *
     * @param <T> Input Value / Result type.
     * @param <O> Operator type.
     */
    @FunctionalInterface
    public interface TriOperator<T, O> {
        /**
         * Applies this function to the given arguments.
         *
         * @param a the first function argument
         * @param b the second function argument
         * @param op operator used
         * @return the function result
         */
        T apply(T a, T b, O op);
    }
}
