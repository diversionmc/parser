package net.thebugmc.parser.pattern;

import net.thebugmc.parser.util.FilePointer;
import net.thebugmc.parser.util.Pointable;

/**
 * Sentence is a real data structure,which can be directly used
 * by an application and no longer belong to the parser
 * (but Sentence knows where in the file it was defined).
 */
public abstract class Sentence extends Pointable {
    /**
     * Construct a sentence at a position.
     *
     * @param ptr Creation position.
     */
    public Sentence(FilePointer ptr) {
        super(ptr);
    }

    /**
     * Useful built-in operation to check whether a given sentence represents an action that does not return value.
     *
     * @return False by default, true if this sentence represents a non-value-returning (void) action.
     */
    public boolean isStatement() {
        return false;
    }
}
