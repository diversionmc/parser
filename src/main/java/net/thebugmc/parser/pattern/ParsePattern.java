package net.thebugmc.parser.pattern;

import net.thebugmc.parser.pattern.PatternResult;
import net.thebugmc.parser.util.ParserException;
import net.thebugmc.parser.Parser;
import net.thebugmc.parser.expression.ExpressionPiece;

import java.util.LinkedList;
import java.util.List;

import static net.thebugmc.parser.util.ParserException.ASSERT;

/**
 * <p>
 * Pattern is a function which consumes Expression Pieces and converts them into a {@link Sentence} list.
 * </p>
 *
 * <p>
 * Patterns either belong to the parser or just exist outside to be used by other patterns.
 * Patterns in the first case will be gone through when trying to parse the whole text.
 * They have IDs by which they can be received from the parser.
 * A parser should have at least one of such a pattern.
 * </p>
 *
 * <p>
 * Pattern takes some amount of pieces,
 * gives this amount and sentence as result (or null on failure),
 * and leaves the rest of pieces to the code that used the pattern.
 * Hence, that code should check whether all pieces were used when required.
 * </p>
 *
 * <p>
 * Patterns in parser-related documents should be defined using following pattern notation:
 *
 * <pre>
 * [_] - piece 0 or 1 times
 * &lt;_&gt; - piece once
 * [_]... - piece 0 or more times
 * &lt;_&gt;... - piece 1 or more times
 * _(_) - group of other pattern (used inside brackets)&gt;
 * a|b - either a or b (used inside brackets)
 * ... - anything / et cetera (used inside brackets)
 * -> _ - possible result (used at the end of a pattern)
 * </pre>
 * <p>
 * The written text (denoted as an underscore) can be anything,
 * but should usually display the piece name
 * (as code variable name or part of the piece structure name),
 * their character content (if it is explicit), or other pattern notation.
 * The patterns can be separated by spaces
 * (reflects how the spaces work in the parsing process).
 * </p>
 *
 * @param <T> Sentence type, matching {@link net.thebugmc.parser.Parser}.
 */
@FunctionalInterface
public interface ParsePattern<T extends Sentence> {
    /**
     * Apply this pattern piece conversion from a given piece list.
     *
     * @param e Pieces to parse.
     * @return {@link net.thebugmc.parser.pattern.PatternResult}, or null if pieces do not match the pattern.
     */
    net.thebugmc.parser.pattern.PatternResult<T> parse(List<net.thebugmc.parser.expression.ExpressionPiece> e);

    /**
     * Try to match at least one pattern from the given patterns.
     *
     * @param expressions Pieces to parse.
     * @param patterns    Patterns to match.
     * @param <T>         Sentence type, matching {@link net.thebugmc.parser.Parser}.
     * @return {@link #parse(List)}, or null if no patterns match.
     */
    @SafeVarargs
    static <T extends Sentence> PatternResult<T> matchOne(List<net.thebugmc.parser.expression.ExpressionPiece> expressions, ParsePattern<T>... patterns) {
        if (expressions.isEmpty()) return null;
        for (var pattern : patterns) {
            var result = pattern.parse(expressions);
            if (result != null) return result;
        }
        return null;
    }

    /**
     * Match all the given patterns or fail.
     *
     * @param expressions Pieces to parse.
     * @param patterns    Patterns to match.
     * @param <T>         Sentence type, matching {@link Parser}.
     * @return List of {@link #matchOne(List, ParsePattern[])}.
     * @throws net.thebugmc.parser.util.ParserException If no patterns match at a given position.
     */
    @SafeVarargs
    static <T extends Sentence> LinkedList<T> match(List<ExpressionPiece> expressions, ParsePattern<T>... patterns) throws net.thebugmc.parser.util.ParserException {
        expressions = new LinkedList<>(expressions);
        var sentences = new LinkedList<T>();

        while (!expressions.isEmpty()) {
            var result = matchOne(expressions, patterns);

            var finalExpressions = expressions;
            net.thebugmc.parser.util.ParserException.ASSERT(result != null, () -> finalExpressions.get(0).pointer(), "No pattern matches \"" + finalExpressions.get(0) + "\"");

            var r = result.result();
            if (r != null) sentences.add(r);
            expressions = expressions.subList(result.length(), expressions.size()); // chop off used expressions
        }

        return sentences;
    }
}
