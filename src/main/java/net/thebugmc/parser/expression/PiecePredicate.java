package net.thebugmc.parser.expression;

import net.thebugmc.parser.util.FilePointer;

/**
 * Function that checks whether a piece is supposed to be created by a given character.
 */
@FunctionalInterface
public interface PiecePredicate {
    /**
     * Check whether a character should create a piece.
     *
     * @param c Character to parse.
     * @param ptr Character position in file.
     * @return True if piece should be made (and associated {@link PieceSupplier} will be called).
     */
    boolean apply(char c, FilePointer ptr);
}
