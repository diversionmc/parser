package net.thebugmc.parser.expression;

import net.thebugmc.parser.util.FilePointer;

import static net.thebugmc.parser.expression.PieceResult.CONTINUE;
import static net.thebugmc.parser.expression.PieceResult.LEAVE;

/**
 * Reads until it is no longer able to parse a number.
 */
public class NumberPiece extends ExpressionPiece {
    @SuppressWarnings("MismatchedQueryAndUpdateOfStringBuilder")
    private final StringBuilder sb = new StringBuilder();
    private double num;

    /**
     * Construct a number piece at a position.
     *
     * @param ptr Creation position.
     */
    public NumberPiece(FilePointer ptr) {
        super(ptr);
    }

    public PieceResult read(char c, FilePointer ptr) {
        if (c == '-' && sb.isEmpty()) {
            sb.append(c);
            return PieceResult.CONTINUE;
        }
        var num = tryNumber(sb.append(c) + "");
        if (num == null) return PieceResult.LEAVE;
        this.num = num;
        return PieceResult.CONTINUE;
    }

    /**
     * Get parsed number value.
     *
     * @return Number Piece value.
     */
    public double number() {
        return num;
    }

    /**
     * Try to decode a double-type number. Accepts hex inputs, etc.
     *
     * @param s String to parse.
     * @return Parsed double or null if the string is not a number.
     * @see Long#decode(String)
     * @see Double#parseDouble(String)
     */
    public static Double tryNumber(String s) {
        if (!s.trim().equals(s)) return null;
        try {
            return Long.decode(s).doubleValue();
        } catch (NumberFormatException e1) {
            try {
                return Double.parseDouble(s);
            } catch (NumberFormatException e2) {
                return null;
            }
        }
    }

    /**
     * Try to parse string as a number and compare to 0 or compare it to string of value "true".
     *
     * @param s String to parse.
     * @return Parsed boolean. If input is a number, then 0 = false, other = true, else true if string is equal to "true" (case-insensitive).
     * @see #tryNumber(String)
     */
    public static boolean tryBoolean(String s) {
        var num = tryNumber(s);
        return num == null ? "true".equalsIgnoreCase(s) : num != 0;
    }
}