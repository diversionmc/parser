package net.thebugmc.parser.expression;

import net.thebugmc.parser.util.FilePointer;

import static net.thebugmc.parser.expression.PieceResult.TAKE;

public abstract class CharPiece extends ExpressionPiece {
    public final char c;

    public CharPiece(FilePointer ptr, char c) {
        super(ptr);
        this.c = c;
    }

    public PieceResult read(char c, FilePointer ptr) {
        return PieceResult.TAKE;
    }

    public String toString() {
        return c + "";
    }
}
