package net.thebugmc.parser.expression;

import net.thebugmc.parser.util.FilePointer;

/**
 * Function that converts a character into a matching piece.
 */
@FunctionalInterface
public interface PieceSupplier {
    /**
     * Apply function to character at a position.
     *
     * @param c   Character to get piece for.
     * @param ptr Character position in file.
     * @return Created piece or null to cancel.
     */
    ExpressionPiece apply(char c, FilePointer ptr);
}
