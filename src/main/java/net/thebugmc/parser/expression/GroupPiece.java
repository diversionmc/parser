package net.thebugmc.parser.expression;

import net.thebugmc.parser.expression.ExpressionPiece;
import net.thebugmc.parser.expression.PieceResult;
import net.thebugmc.parser.util.FilePointer;

import java.util.List;

/**
 * List of pieces (called group content). Does not read any characters.
 */
public abstract class GroupPiece extends net.thebugmc.parser.expression.ExpressionPiece {
    private final List<net.thebugmc.parser.expression.ExpressionPiece> content;

    /**
     * Construct a group piece.
     *
     * @param ptr     Creation pointer; usually left boundary position.
     * @param content Pieces inside the group.
     */
    public GroupPiece(FilePointer ptr, List<net.thebugmc.parser.expression.ExpressionPiece> content) {
        super(ptr);
        this.content = content;
    }

    public final PieceResult read(char c, FilePointer ptr) {
        return null;
    }

    /**
     * Get all pieces that are inside the group.
     *
     * @return Grouped pieces.
     */
    public List<ExpressionPiece> content() {
        return content;
    }

    public String toString() {
        return getClass().getSimpleName() + "{content=" + content + '}';
    }
}
