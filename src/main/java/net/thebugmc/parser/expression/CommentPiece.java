package net.thebugmc.parser.expression;

import net.thebugmc.parser.expression.PieceResult;
import net.thebugmc.parser.util.FilePointer;

import static net.thebugmc.parser.expression.PieceResult.*;

/**
 * Can be inline or multiline, reads until newline or comment end.
 * Saves piece before comment and goes back to it once comment ends.
 */
public class CommentPiece extends ExpressionPiece {
    private final boolean inline;
    private final ExpressionPiece original;

    private char last;

    /**
     * Construct a comment piece.
     *
     * @param ptr Creation position.
     * @param inline Whether this comment piece should stop at newline or at comment end.
     * @param original Piece to continue parsing with at the end of comment or null to discard both pieces.
     */
    public CommentPiece(FilePointer ptr, boolean inline, ExpressionPiece original) {
        super(ptr);
        this.inline = inline;
        this.original = original;
    }

    public PieceResult read(char c, FilePointer ptr) {
        if (inline) return c == '\n' ? REPLACE_LEAVE : CONTINUE;

        if (last == '*' && c == '/') return REPLACE_TAKE;
        last = c;
        return CONTINUE;
    }

    public ExpressionPiece replace(FilePointer ptr) {
        return original;
    }
}
