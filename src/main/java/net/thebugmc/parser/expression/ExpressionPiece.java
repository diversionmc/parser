package net.thebugmc.parser.expression;

import net.thebugmc.parser.expression.PiecePredicate;
import net.thebugmc.parser.expression.PieceResult;
import net.thebugmc.parser.util.FilePointer;
import net.thebugmc.parser.util.Pointable;

/**
 * <p>
 * Expression Pieces are the units of the text format.
 * They represent single characters or sequences of characters
 * digested into structural form by first parsing pass.
 * </p>
 *
 * <p>
 * A piece usually features a {@link PiecePredicate} determining which piece type
 * should start processing at a current point in file given a character.
 * There must be only one valid piece type at a given point.
 * Piece never starts with a whitespace character (space, tab, newline, etc),
 * when there is no currently set piece, the whitespace characters are skipped.
 * </p>
 */
public abstract class ExpressionPiece extends Pointable {
    /**
     * Construct a piece at a position.
     *
     * @param ptr Creation position.
     */
    public ExpressionPiece(FilePointer ptr) {
        super(ptr);
    }

    /**
     * Parse a character by this piece.
     *
     * @param c   Character to parse.
     * @param ptr Character position in file.
     * @return {@link net.thebugmc.parser.expression.PieceResult}
     */
    public abstract net.thebugmc.parser.expression.PieceResult read(char c, FilePointer ptr);

    /**
     * Called whenever {@link net.thebugmc.parser.expression.PieceResult#REPLACE_LEAVE} or {@link PieceResult#REPLACE_TAKE} are used.
     *
     * @param ptr Position when replace was used.
     * @return Another piece to continue parsing with, or null to discard current piece.
     */
    public ExpressionPiece replace(FilePointer ptr) {
        return null;
    }
}
