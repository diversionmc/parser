package net.thebugmc.parser.expression;

import net.thebugmc.parser.Parser;
import net.thebugmc.parser.util.FilePointer;

import java.util.function.Consumer;

/**
 * Piece result shows whether a piece should stop consuming characters or continue.
 */
public enum PieceResult {
    /**
     * Stop reading and consume the supplied character.
     * The parse pointer will be incremented by 1.
     * Default piece result when a null is given.
     */
    TAKE,
    /**
     * Stop reading and leave the supplied character.
     * The parse pointer will stay as is, leaving the current character for the next piece iteration.
     */
    LEAVE,
    /**
     * Stop reading and consume the supplied character, call {@link ExpressionPiece#replace(FilePointer)}
     *   and proceed to read next characters in the new piece.
     * Behaves same as {@link #TAKE} if replacement piece is null, except does not call {@link Parser#pieceFinish(Consumer)}.
     */
    REPLACE_TAKE,
    /**
     * Stop reading and leave the supplied character, call {@link ExpressionPiece#replace(FilePointer)}
     *   and proceed to read that character in the new piece.
     * Behaves same as {@link #LEAVE} if replacement piece is null, except does not call {@link Parser#pieceFinish(Consumer)}.
     */
    REPLACE_LEAVE,
    /**
     * Ask for another character to consume.
     * The parse pointer will be incremented by 1 and {@link ExpressionPiece#read(char, FilePointer)} will be called again.
     */
    CONTINUE,
}
