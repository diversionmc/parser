package net.thebugmc.parser;

import net.thebugmc.parser.group.GroupSupplier;
import net.thebugmc.parser.group.Grouper;
import net.thebugmc.parser.pattern.ParsePattern;
import net.thebugmc.parser.pattern.Sentence;
import net.thebugmc.parser.util.FilePointer;
import net.thebugmc.parser.expression.ExpressionPiece;
import net.thebugmc.parser.expression.PiecePredicate;
import net.thebugmc.parser.expression.PieceResult;
import net.thebugmc.parser.expression.PieceSupplier;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static java.lang.Math.exp;
import static java.lang.Math.max;
import static java.util.stream.Collectors.toList;
import static net.thebugmc.parser.expression.PieceResult.*;
import static net.thebugmc.parser.pattern.ParsePattern.match;
import static net.thebugmc.parser.util.ParserException.ASSERT;

/**
 * Parser - convert any written language into custom format
 *
 * @param <T> Parsed text output sentence type.
 */
public final class Parser<T extends Sentence> {
    //
    // Boilerplate
    //

    private String name = hashCode() + "";
    private String text = null;

    private final List<ParsePiece> pieces = new LinkedList<>();
    private final List<Grouper<?, ?>> groups = new LinkedList<>();
    private final Map<String, ParsePattern<T>> patterns = new LinkedHashMap<>();

    private Consumer<net.thebugmc.parser.expression.ExpressionPiece> pieceFinish = null;
    private Runnable pre = null;
    private FilePointer end;

    private static record ParsePiece(net.thebugmc.parser.expression.PiecePredicate check, net.thebugmc.parser.expression.PieceSupplier supplier) {
        public boolean check(char c, FilePointer ptr) {
            return check.apply(c, ptr);
        }

        public net.thebugmc.parser.expression.ExpressionPiece apply(char c, FilePointer ptr) {
            return supplier.apply(c, ptr);
        }
    }

    /**
     * Create Parser without any input attached and automatically set name.
     */
    public Parser() {
    }

    /**
     * Create Parser with a title.
     *
     * @param name Text title; usually filename.
     */
    public Parser(String name) {
        this.name = name;
    }

    /**
     * Create Parser of a text with a title.
     *
     * @param name Text title; usually filename.
     * @param text Input text to parse.
     */
    public Parser(String name, String text) {
        this(name);
        text(text);
    }

    /**
     * Create Parser from an input stream with a title.
     *
     * @param name Text title; usually filename.
     * @param is   Input stream to get text from to parse.
     * @throws IOException If stream reading error occurs.
     */
    public Parser(String name, InputStream is) throws IOException {
        this(name);
        readFrom(is);
    }

    /**
     * Create Parser from a file.
     *
     * @param f File to parse.
     * @throws IOException If file reading error occurs.
     */
    public Parser(File f) throws IOException {
        readFrom(f);
    }

    /**
     * Create Parser without any input attached and automatically set name.
     *
     * @param <T> Parsed text output sentence type.
     * @return {@link #Parser()}
     */
    public static <T extends Sentence> Parser<T> parser() {
        return new Parser<>();
    }

    /**
     * Create Parser of a text with a title.
     *
     * @param name Text title; usually filename.
     * @param <T>  Parsed text output sentence type.
     * @return {@link #Parser(String)}
     */
    public static <T extends Sentence> Parser<T> parser(String name) {
        return new Parser<>(name);
    }

    /**
     * Create Parser of a text with a title.
     *
     * @param name Text title; usually filename.
     * @param text Input text to parse.
     * @param <T>  Parsed text output sentence type.
     * @return {@link #Parser(String, String)}
     */
    public static <T extends Sentence> Parser<T> parser(String name, String text) {
        return new Parser<>(name, text);
    }

    /**
     * Create Parser from an input stream with a title.
     *
     * @param name Text title; usually filename.
     * @param is   Input stream to get text from to parse.
     * @param <T>  Parsed text output sentence type.
     * @return {@link #Parser(String, InputStream)}
     * @throws IOException If stream reading error occurs.
     */
    public static <T extends Sentence> Parser<T> parser(String name, InputStream is) throws IOException {
        return new Parser<>(name, is);
    }

    /**
     * Create Parser from a file.
     *
     * @param f   File to parse.
     * @param <T> Parsed text output sentence type.
     * @return {@link #Parser(File)}
     * @throws IOException If file reading error occurs.
     */
    public static <T extends Sentence> Parser<T> parser(File f) throws IOException {
        return new Parser<>(f);
    }

    /**
     * Get title used on creation of this parser. If title was not specified, it is {@link #hashCode()}.
     *
     * @return Text title; usually filename.
     */
    public String name() {
        return name;
    }

    /**
     * Get text that was inputted into this Parser.
     *
     * @return Input text to parse.
     */
    public String text() {
        return text;
    }

    /**
     * Set text to parse.
     *
     * @param text Input text to parse.
     */
    public Parser<T> text(String text) {
        ASSERT(text != null, "Parser text is null");
        this.text = text + '\n';
        end = new FilePointer(name, max(1, text.split("\n").length), 1);
        return this;
    }

    /**
     * Set text to parse from a file, as well as text title.
     *
     * @param f File to parse.
     * @throws IOException If file reading error occurs.
     */
    public Parser<T> readFrom(File f) throws IOException {
        ASSERT(f != null, "Parser file is null");

        var s = new StringBuilder();
        if (f.exists()) {
            String text;

            try (var br = new BufferedReader(new InputStreamReader(new FileInputStream(f)))) {
                while ((text = br.readLine()) != null) s.append(text).append('\n');
            }
        }
        var n = f.getName();
        int i = n.lastIndexOf('.');
        this.name = i < 0 ? n : n.substring(0, i);
        text(s.append('\n').toString());

        return this;
    }

    /**
     * Set text to parse from a stream.
     *
     * @param is Input stream to get text from to parse.
     * @throws IOException If stream reading error occurs.
     */
    public Parser<T> readFrom(InputStream is) throws IOException {
        ASSERT(is != null, "Parser stream is null");

        var s = new StringBuilder();
        String text;

        var br = new BufferedReader(new InputStreamReader(is)); // do not close foreign stream
        while ((text = br.readLine()) != null) s.append(text).append('\n');

        text(s.append('\n').toString());
        return this;
    }

    //
    // API
    //

    /**
     * Get file pointer pointing to the end of the file (line += 1, column = 1).
     *
     * @return End file pointer.
     */
    public FilePointer end() {
        return end;
    }

    /**
     * Set the first action to perform on {@link #build()} before starting to parse text.
     *
     * @param pre Action to run.
     */
    public Parser<T> pre(Runnable pre) {
        this.pre = pre;
        return this;
    }

    /**
     * Add a parse piece that is always accepted.
     *
     * @param supplier Piece supplier, null is same as not accepting a piece.
     */
    public Parser<T> piece(net.thebugmc.parser.expression.PieceSupplier supplier) {
        return piece((c, ptr) -> true, supplier);
    }

    /**
     * Add a parse piece.
     *
     * @param check    Check whether the piece is acceptable for a character at a position.
     * @param supplier Piece supplier, null is same as not accepting a piece.
     */
    public Parser<T> piece(PiecePredicate check, PieceSupplier supplier) {
        ASSERT(supplier != null, "Parser piece supplier is null");
        pieces.add(new ParsePiece(check, supplier));
        return this;
    }

    /**
     * Add an action to run after a piece is completed.
     *
     * @param event Action to run.
     */
    public Parser<T> pieceFinish(Consumer<net.thebugmc.parser.expression.ExpressionPiece> event) {
        pieceFinish = event;
        return this;
    }

    /**
     * Create a grouper for specific pieces.
     *
     * @param left     Group opening piece.
     * @param right    Group closing piece.
     * @param supplier Group creator.
     * @param <L>      Opening piece type.
     * @param <R>      Closing piece type.
     */
    public <L extends net.thebugmc.parser.expression.ExpressionPiece, R extends net.thebugmc.parser.expression.ExpressionPiece> Parser<T> group(Predicate<net.thebugmc.parser.expression.ExpressionPiece> left,
                                                                                                                                                Predicate<net.thebugmc.parser.expression.ExpressionPiece> right,
                                                                                                                                                GroupSupplier<L, R> supplier) {
        groups.add(new Grouper<>(left, right, supplier));
        return this;
    }

    /**
     * Add a piece to sentence converter.
     *
     * @param id      Name of the pattern.
     * @param pattern The pattern.
     */
    public Parser<T> pattern(String id, ParsePattern<T> pattern) {
        ASSERT(id != null && !id.isBlank(), "Pattern ID cannot be null or empty");
        patterns.put(id, pattern);
        return this;
    }

    /**
     * Get a pattern that was already added to this parser.
     *
     * @param id Name of the pattern.
     */
    public ParsePattern<T> pattern(String id) {
        return patterns.get(id);
    }

    /**
     * Get all patterns that were added to this parser.
     *
     * @return Array of the patterns to use in matchOne().
     */
    @SuppressWarnings("unchecked")
    public ParsePattern<T>[] patterns() {
        return patterns.values().toArray(ParsePattern[]::new);
    }

    /**
     * Convert input text into a list of usable sentences.
     *
     * @return List of sentences.
     */
    public List<T> build() {
        return match(buildExpressionsGrouped(), patterns());
    }

    /**
     * Convert input text into a list of ExpressionPieces.
     *
     * @return List of ExpressionPieces.
     */
    public List<net.thebugmc.parser.expression.ExpressionPiece> buildExpressions() {
        if (text.isBlank()) return Collections.emptyList();
        if (pre != null) pre.run();

        // Convert text into expressions
        List<net.thebugmc.parser.expression.ExpressionPiece> expressions = new LinkedList<>();
        net.thebugmc.parser.expression.ExpressionPiece last = null;
        FilePointer ptr = null;

        int pos = 0, len = text.length(),
            line = 1, col = 1;
        while (pos < len) {
            ptr = new FilePointer(name, line, col);
            char c = text.charAt(pos);

            // Read to current character
            if (last != null) {
                var r = last.read(c, ptr); // null is same as TAKE

                // LEAVE and REPLACE_LEAVE do not use up a character,
                //  TAKE, REPLACE_TAKE and CONTINUE do
                if (r != PieceResult.LEAVE && r != PieceResult.REPLACE_LEAVE) {
                    pos++;
                    if (c == '\n') {
                        line++;
                        col = 1;
                    } else col++;
                }

                // REPLACE_LEAVE and REPLACE_TAKE end previous expression, null is allowed
                if (r == PieceResult.REPLACE_LEAVE || r == PieceResult.REPLACE_TAKE) last = last.replace(ptr);
                    // CONTINUE does not end an expression
                else if (r != PieceResult.CONTINUE) {
                    if (pieceFinish != null) pieceFinish.accept(last);
                    expressions.add(last);
                    last = null;
                }
                continue;
            }

            // Skip dangling whitespaces
            if (Character.isWhitespace(c)) {
                pos++;
                if (c == '\n') {
                    line++;
                    col = 1;
                } else col++;
                continue;
            }

            // Find new piece
            FilePointer finalPtr = ptr;
            var found = pieces.stream()
                .filter(piece -> piece.check(c, finalPtr))
                .map(piece -> piece.apply(c, finalPtr))
                .filter(Objects::nonNull)
                .toList();
            ASSERT(found.size() != 0, () -> finalPtr, "Invalid symbol '" + c + "'");
            ASSERT(found.size() == 1, () -> finalPtr, "Ambiguous symbol '" + c + "'");
            last = found.get(0);
        }

        FilePointer finalPtr = ptr;
        ASSERT(last == null, () -> finalPtr, "Expression did not end (last: " + last + ")");

        return expressions;
    }

    /**
     * Convert input text into a list of ExpressionPieces, grouped up with registered groupers.
     *
     * @return List of ExpressionPieces with group ExpressionPieces present.
     */
    public List<net.thebugmc.parser.expression.ExpressionPiece> buildExpressionsGrouped() {
        return group(buildExpressions());
    }

    /**
     * Group up ExpressionPieces in the given list into ExpressionPieces which represent such groups.
     *
     * @return List of ExpressionPieces with group ExpressionPieces present.
     */
    public List<net.thebugmc.parser.expression.ExpressionPiece> group(List<net.thebugmc.parser.expression.ExpressionPiece> content) {
        var result = new LinkedList<ExpressionPiece>();
        if (content.size() == 0) return result;

        Grouper<?, ?> startGrouper = null;
        int start = 0, // index of outer group start (when depth turned from 0 to 1)
            depth = 0; // inner group depth, 0 = no group, 1 = outer group, 2... = inner groups
        for (int i = 0, limit = content.size(); i < limit; i++) {
            var current = content.get(i);
            if (depth > 0) {
                if (startGrouper.start(current)) depth++; // current is startPiece
                else if (startGrouper.end(current)) { // current is endPiece
                    var startPiece = content.get(start);
                    if (--depth == 0) // outer group ended
                        current = startGrouper.group( // will have result.add(current) called later
                            startPiece, current,
                            group(content.subList(start + 1, i))); // all pieces will be inside the group
                }
            } else for (var grouper : groups)
                if (grouper.start(current)) {
                    startGrouper = grouper;
                    start = i;
                    depth++;
                    break;
                }
            if (depth == 0) result.add(current); // add piece to result if we are not in a group (anymore)
        }

        int finalStart = start;
        ASSERT(depth == 0, () -> content.get(finalStart).pointer(), "unbalanced group");

        return result;
    }
}